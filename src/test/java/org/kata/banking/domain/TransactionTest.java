package org.kata.banking.domain;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class TransactionTest {

  @Test
  @DisplayName("Test la récupération du numéro d'une transaction qui a eu lieu")
  void transactionNumber() {
    // Given

    // When

    // Then
  }

  @Test
  @DisplayName("Test la récupération d'une date de transaction qui a eu lieu")
  void transactionDate() {
    // Given

    // When

    // Then
  }

  @Test
  @DisplayName("Test la récupération de la balance après qu'une transaction est eu lieu")
  void balanceAfterTransaction() {
    // Given

    // When

    // Then
  }

  @Test
  @DisplayName("Test la récupération de la balance avant qu'une transaction est eu lieu")
  void balanceBeforeTransaction() {
    // Given

    // When

    // Then
  }
}