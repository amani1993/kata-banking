package org.kata.banking.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class AmountTest {

  @BeforeEach
  void setUp() {
  }

  @Test
  @DisplayName("Test l'ajout l'addition deux 2 montants")
  void plus() {
    // Given

    // When

    // Then
  }

  @Test
  @DisplayName("Test la récupération de la valeur négative d'un montant")
  void negative() {
    // Given

    // When

    // Then
  }

  @Test
  @DisplayName("Test la comparaison du plus grand entre 2 montants")
  void isGreaterThan() {
    // Given

    // When

    // Then
  }

  @Test
  @DisplayName("Test la représentation textuel d'un montant")
  void moneyRepresentation() {
    // Given

    // When

    // Then
  }
}