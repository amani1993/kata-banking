package org.kata.banking.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class AccountTest {

  @BeforeEach
  void setUp() {
  }

  @Test
  @DisplayName("Test la récupération de la référence unique d'un compte")
  void accountReference() {
    // Given

    // When

    // Then
  }

  @Test
  @DisplayName("Test le dépôt d'argent dans un compte")
  void deposit() {
    // Given

    // When

    // Then
  }

  @Test
  @DisplayName("Test le retrait d'argent dans un compte")
  void withdraw() {
    // Given

    // When

    // Then
  }

  @Test
  @DisplayName("Test la récupération et la cohérence de la balance d'un compte")
  void balance() {
    // Given

    // When

    // Then
  }

  @Test
  @DisplayName("Test la récupération actuel du montant d'un compte")
  void printStatement() {
    // Given

    // When

    // Then
  }
}