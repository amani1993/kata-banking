package org.kata.banking.usecase;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class MakeDepositTest {

  @BeforeEach
  void setUp() {
  }

  @Test
  @DisplayName("Test le dépôt d'argent dans un compte récupérer dans une DB à partir de sa référence unique")
  void makeDeposit() {
    // Given

    // When

    // Then
  }

  @Test
  @DisplayName("Test l'impossibilité d'effectuer un dépôt d'argent dans un compte inexistant")
  void impossibleMakeDeposit() {
    // Given

    // When

    // Then
  }
}