package org.kata.banking.usecase;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class MakeWithdrawalTest {

  @BeforeEach
  void setUp() {
  }

  @Test
  @DisplayName("Test le retrait d'argent dans un compte récupérer dans une DB à partir de sa référence unique")
  void makeWithdrawal() {
    // Given

    // When

    // Then
  }

  @Test
  @DisplayName("Test l'impossibilité d'effectuer un retrait d'argent dans un compte qui ne possède pas une balance suffisante")
  void impossibleMakeWithdrawal() {
    // Given

    // When

    // Then
  }
}