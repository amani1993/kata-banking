package org.kata.banking;

public interface Presenter<T> {

  void showData(T data);

  boolean availableData();
}
