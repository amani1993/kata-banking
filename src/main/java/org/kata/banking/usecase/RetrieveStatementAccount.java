package org.kata.banking.usecase;

import org.kata.banking.UseCase;

/**
 * <p>Cas d'utilisation pour l'affichage sur la console du le relever d'un {@link
 * org.kata.banking.domain.Account}.</p>
 *
 * @author Christian Amani 2021-01-11
 */
public class RetrieveStatementAccount implements UseCase {


  @Override
  public <T> void perform(T command) {

  }
}
