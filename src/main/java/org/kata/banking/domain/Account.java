package org.kata.banking.domain;

public interface Account {

  /**
   * @return Retourne la référence unique du compte
   */
  String accountReference();

  /**
   * <p>Effectue un dépôt d'argent sur le compte</p>
   *
   * @param amount Montant à déposer
   */
  void deposit(Amount amount);

  /**
   * <p>Effectue un retrait d'argent sur le compte</p>
   *
   * @param amount Montant à retirer
   */
  void withdraw(Amount amount);

  /**
   * <p>Renvoie la balance actuel du compt</p>
   *
   * @return Retourne la balance actuel
   */
  Amount balance();

  /**
   * @return Le montant actuel du compte
   */
  String printStatement();
}
