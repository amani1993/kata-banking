package org.kata.banking.domain;

public interface Transaction {

  /**
   * <p>Retourne le numéro de la transaction.</p>
   *
   * @return Le numéro de la transaction
   */
  long transactionNumber();

  /**
   * <p>Retourne la date de la transaction.</p>
   *
   * @return La date de la transaction
   */
  String transactionDate();

  /**
   * <p>Retourne la balance après la transaction.</p>
   *
   * @return La balance après la transaction
   */
  Amount balanceAfterTransaction();

  /**
   * <p>Retourne la balance avant la transaction.</p>
   *
   * @return La balance avant la transaction
   */
  Amount balanceBeforeTransaction();
}
