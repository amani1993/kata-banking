package org.kata.banking;

public interface UseCase {

  <T> void perform(T command);
}
