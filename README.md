# Bank account kata

Think of your personal bank account experience When in doubt, go for the simplest solution

## Requirements

Deposit and Withdrawal  
Transfer  
Account statement (date, amount, balance)  
Statement printing  
Statement filters (just deposits, withdrawal, date)

## Use cases

We have 3 use cases which are listed below :

1. Making a deposit to an account
2. Making a withdrawal from an account
3. Retrieving a statement of account

## The Rules

1. No getters/setters/properties
2. All your classes must be immutable. Test classes are not affected.
2. Use only constructor parameter
3. Respect the following principles
   * Principle of DRY
   * Principle of KISS
   * Principle of YAGNI
   * Principle of Law of Dameter (LoD)
4. No third-party libraries are permitted.

## Directory

````
|————src/main/java
              |—————org/kaka/banking
                             |————————domain
                             |————————usecase
                             |————————command
                             |————————exception
````

| Package  | Content |
|---       |---|
| domain  | The business entities, business value objects, business services, business exceptions  |
| usecase | Use cases of the application  |
| command   | The commands containing the information for the execution of use cases. The commands are used in the use cases.  |
| exception   | Exceptions for use cases and domain |

### My (*unfinished*) solution

Started from defining an acceptance test:

> Given a client makes a deposit of 1000 on 10-01-2012  
And a deposit of 2000 on 13-01-2012  
And a withdrawal of 500 on 14-01-2012  
When she prints her bank statement  
Then she would see  
date || credit || debit || balance  
14/01/2012 || || 500.00 || 2500.00   
13/01/2012 || 2000.00 || || 3000.00  
10/01/2012 || 1000.00 || || 1000.00

Do it yourself first and then compare the solutions.
